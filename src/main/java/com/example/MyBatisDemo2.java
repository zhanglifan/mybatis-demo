package com.example;


import com.example.mapper.UserMapper;
import com.example.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Mybatis 代理开发
 */
public class MyBatisDemo2 {

    public static void main(String[] args) throws IOException {

        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3. 执行sql
        //List<User> users = sqlSession.selectList("test.selectAll");
        System.out.println("sqlSession"+sqlSession);
        //3.1 获取UserMapper接口的代理对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
//        List<User> users = userMapper.selectAll();
//        User user = userMapper.selectById(2);
//        user.setUsername("测试数据");
//        user.setGender("");
//        User user = new User();
//        user.setId(29);
//        user.setAddr("ceshi");
//        user.setGender("女");
//        user.setUsername("hahha");
//        user.setPassword(null);
//        userMapper.Update(user);
        sqlSession.commit();

        List<User> Ausers = userMapper.selectAll();
        System.out.println(Ausers.size());
        System.out.println(Ausers);
        //4. 释放资源
        sqlSession.close();
    }
}
