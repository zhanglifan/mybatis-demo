package com.example.mapper;

import com.example.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.Collection;
import java.util.List;

public interface UserMapper {

    List<User> selectAll();

    void deleteUser(int id);
    void updateUser(User ur);
    void insertUser(User ur);


    @Select("select * from tb_user where id  = #{id}")
    User selectById(int id);
    @Insert("insert into tb_user " +
            "(username, password, gender, addr)  VALUES " +
            "(#{username}, #{password}, #{gender}, #{addr})")
    void Insert(User ur);
    @Delete("delete from tb_user where id = #{id}")
    void Delete(int id);
    //基础update 语句
    //@Update("update tb_user SET username = #{username}, password = #{password}, gender = #{gender},addr = #{addr} where id = #{id}")
    //加入非空判断后的查询语句
    @Update("update tb_user SET username = CASE WHEN #{username} IS NOT NULL THEN #{username} ELSE username END,\n" +
            "    password = CASE WHEN #{password} IS NOT NULL THEN #{password} ELSE password END,\n" +
            "    gender = CASE WHEN #{gender} IS NOT NULL THEN #{gender} ELSE gender END,\n" +
            "    addr = CASE WHEN #{addr} IS NOT NULL THEN #{addr} ELSE addr END where id = #{id}")
    void Update(User ur);

    /*

      MyBatis 参数封装：
        * 单个参数：
            1. POJO类型：直接使用，属性名 和 参数占位符名称 一致
            2. Map集合：直接使用，键名 和 参数占位符名称 一致
            3. Collection：封装为Map集合，可以使用@Param注解，替换Map集合中默认的arg键名
                map.put("arg0",collection集合);
                map.put("collection",collection集合);
            4. List：封装为Map集合，可以使用@Param注解，替换Map集合中默认的arg键名
                map.put("arg0",list集合);
                map.put("collection",list集合);
                map.put("list",list集合);
            5. Array：封装为Map集合，可以使用@Param注解，替换Map集合中默认的arg键名
                map.put("arg0",数组);
                map.put("array",数组);
            6. 其他类型：直接使用
        * 多个参数：封装为Map集合,可以使用@Param注解，替换Map集合中默认的arg键名
            map.put("arg0",参数值1)
            map.put("param1",参数值1)
            map.put("param2",参数值2)
            map.put("agr1",参数值2)
            ---------------@Param("username")
            map.put("username",参数值1)
            map.put("param1",参数值1)
            map.put("param2",参数值2)
            map.put("agr1",参数值2)

     */
    User select(@Param("username") String username, String password);
    User select(Collection collection);













}
